#import "../theme.typ": *
#import "../highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(step 10)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(step 10)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(raw("(print \"At \" INDEX \": \" top.data)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(step 10)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(step 10)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(5, [#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(step 10)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(6, [#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(step 10)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(7, [#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(step 10)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(print \"At \" INDEX \": \" top.data)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(8, [#box(text(raw("(print \"At \" INDEX \": \" top.data)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(step 10)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(print \"At \" INDEX \": \" top.data)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
