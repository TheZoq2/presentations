#import "../theme.typ": *
#import "../highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(raw("entity cpu(clk: clock,", lang: "spade"))#box(raw(" wb: Wishbone", lang: "spade"))#box(raw(") {..}", lang: "spade"))\
#box(raw("entity accelerator(clk: clock, wb: Wishbone) {..}", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("entity wb_harness(clk: clock, rst: bool) {", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("let (wb1, wb1inv) = port;", lang: "spade"))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("  let (wb2, wb2inv) = port;", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("let _ = inst wishbone_arbiter(", lang: "spade"))\
#box(raw("    .., wb1inv, wb2inv, ..", lang: "spade"))\
#box(raw("  );", lang: "spade"))\
#box(raw("  let o1 = inst cpu(..,", lang: "spade"))#box(raw(" wb1", lang: "spade"))#box(raw(", ..);", lang: "spade"))\
#box(raw("  let o2 = inst accelerator(.., rst,", lang: "spade"))#box(raw(" wb2", lang: "spade"))#box(raw(", ..);", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("}", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(raw("entity cpu(clk: clock,", lang: "spade"))#box(raw(" wb: Wishbone", lang: "spade"))#box(raw(") {..}", lang: "spade"))\
#box(raw("entity accelerator(clk: clock, wb: Wishbone) {..}", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("entity wb_harness(clk: clock, rst: bool) {"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let (wb1, wb1inv) = port;"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  let (wb2, wb2inv) = port;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let _ = inst wishbone_arbiter("), fill: unfocused_color))\
#box(text(raw("    .., wb1inv, wb2inv, .."), fill: unfocused_color))\
#box(text(raw("  );"), fill: unfocused_color))\
#box(text(raw("  let o1 = inst cpu(..,"), fill: unfocused_color))#box(text(raw(" wb1"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))\
#box(text(raw("  let o2 = inst accelerator(.., rst,"), fill: unfocused_color))#box(text(raw(" wb2"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(text(raw("entity cpu(clk: clock,"), fill: unfocused_color))#box(raw(" wb: Wishbone", lang: "spade"))#box(text(raw(") {..}"), fill: unfocused_color))\
#box(text(raw("entity accelerator(clk: clock, wb: Wishbone) {..}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("entity wb_harness(clk: clock, rst: bool) {"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let (wb1, wb1inv) = port;"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  let (wb2, wb2inv) = port;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let _ = inst wishbone_arbiter("), fill: unfocused_color))\
#box(text(raw("    .., wb1inv, wb2inv, .."), fill: unfocused_color))\
#box(text(raw("  );"), fill: unfocused_color))\
#box(text(raw("  let o1 = inst cpu(..,"), fill: unfocused_color))#box(text(raw(" wb1"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))\
#box(text(raw("  let o2 = inst accelerator(.., rst,"), fill: unfocused_color))#box(text(raw(" wb2"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(text(raw("entity cpu(clk: clock,"), fill: unfocused_color))#box(text(raw(" wb: Wishbone"), fill: unfocused_color))#box(text(raw(") {..}"), fill: unfocused_color))\
#box(text(raw("entity accelerator(clk: clock, wb: Wishbone) {..}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("entity wb_harness(clk: clock, rst: bool) {", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let (wb1, wb1inv) = port;"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  let (wb2, wb2inv) = port;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let _ = inst wishbone_arbiter("), fill: unfocused_color))\
#box(text(raw("    .., wb1inv, wb2inv, .."), fill: unfocused_color))\
#box(text(raw("  );"), fill: unfocused_color))\
#box(text(raw("  let o1 = inst cpu(..,"), fill: unfocused_color))#box(text(raw(" wb1"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))\
#box(text(raw("  let o2 = inst accelerator(.., rst,"), fill: unfocused_color))#box(text(raw(" wb2"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(5, [#box(text(raw("entity cpu(clk: clock,"), fill: unfocused_color))#box(text(raw(" wb: Wishbone"), fill: unfocused_color))#box(text(raw(") {..}"), fill: unfocused_color))\
#box(text(raw("entity accelerator(clk: clock, wb: Wishbone) {..}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("entity wb_harness(clk: clock, rst: bool) {"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("let (wb1, wb1inv) = port;", lang: "spade"))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("  let (wb2, wb2inv) = port;", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let _ = inst wishbone_arbiter("), fill: unfocused_color))\
#box(text(raw("    .., wb1inv, wb2inv, .."), fill: unfocused_color))\
#box(text(raw("  );"), fill: unfocused_color))\
#box(text(raw("  let o1 = inst cpu(..,"), fill: unfocused_color))#box(text(raw(" wb1"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))\
#box(text(raw("  let o2 = inst accelerator(.., rst,"), fill: unfocused_color))#box(text(raw(" wb2"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(6, [#box(text(raw("entity cpu(clk: clock,"), fill: unfocused_color))#box(text(raw(" wb: Wishbone"), fill: unfocused_color))#box(text(raw(") {..}"), fill: unfocused_color))\
#box(text(raw("entity accelerator(clk: clock, wb: Wishbone) {..}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("entity wb_harness(clk: clock, rst: bool) {"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("let (wb1, wb1inv) = port;", lang: "spade"))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("  let (wb2, wb2inv) = port;", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let _ = inst wishbone_arbiter("), fill: unfocused_color))\
#box(text(raw("    .., wb1inv, wb2inv, .."), fill: unfocused_color))\
#box(text(raw("  );"), fill: unfocused_color))\
#box(text(raw("  let o1 = inst cpu(..,"), fill: unfocused_color))#box(raw(" wb1", lang: "spade"))#box(text(raw(", ..);"), fill: unfocused_color))\
#box(text(raw("  let o2 = inst accelerator(.., rst,"), fill: unfocused_color))#box(raw(" wb2", lang: "spade"))#box(text(raw(", ..);"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(7, [#box(text(raw("entity cpu(clk: clock,"), fill: unfocused_color))#box(text(raw(" wb: Wishbone"), fill: unfocused_color))#box(text(raw(") {..}"), fill: unfocused_color))\
#box(text(raw("entity accelerator(clk: clock, wb: Wishbone) {..}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("entity wb_harness(clk: clock, rst: bool) {"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let (wb1, wb1inv) = port;"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  let (wb2, wb2inv) = port;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("let _ = inst wishbone_arbiter(", lang: "spade"))\
#box(raw("    .., wb1inv, wb2inv, ..", lang: "spade"))\
#box(raw("  );", lang: "spade"))\
#box(raw("  let o1 = inst cpu(..,", lang: "spade"))#box(raw(" wb1", lang: "spade"))#box(raw(", ..);", lang: "spade"))\
#box(raw("  let o2 = inst accelerator(.., rst,", lang: "spade"))#box(raw(" wb2", lang: "spade"))#box(raw(", ..);", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(8, [#box(text(raw("entity cpu(clk: clock,"), fill: unfocused_color))#box(text(raw(" wb: Wishbone"), fill: unfocused_color))#box(text(raw(") {..}"), fill: unfocused_color))\
#box(text(raw("entity accelerator(clk: clock, wb: Wishbone) {..}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("entity wb_harness(clk: clock, rst: bool) {"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("#[wal_trace(clk=clk, rst=rst)]", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let (wb1, wb1inv) = port;"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("#[wal_trace(clk=clk, rst=rst)]", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  let (wb2, wb2inv) = port;"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("let _ = inst wishbone_arbiter("), fill: unfocused_color))\
#box(text(raw("    .., wb1inv, wb2inv, .."), fill: unfocused_color))\
#box(text(raw("  );"), fill: unfocused_color))\
#box(text(raw("  let o1 = inst cpu(..,"), fill: unfocused_color))#box(text(raw(" wb1"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))\
#box(text(raw("  let o2 = inst accelerator(.., rst,"), fill: unfocused_color))#box(text(raw(" wb2"), fill: unfocused_color))#box(text(raw(", ..);"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
