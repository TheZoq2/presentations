#import "../theme.typ": *
#import "../highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(text(raw("top.clk"), fill: unfocused_color))\
#box(text(raw("top.accelerator.state"), fill: unfocused_color))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.pc"), fill: unfocused_color))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(text(raw("top.clk"), fill: unfocused_color))\
#box(text(raw("top.accelerator.state"), fill: unfocused_color))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.pc"), fill: unfocused_color))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(raw("top.clk", lang: ""))\
#box(raw("top.accelerator.state", lang: ""))\
#box(raw("top.accelerator.wb_", lang: ""))#box(raw("STB", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.accelerator.wb_", lang: ""))#box(raw("ACK", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.accelerator.wb_", lang: ""))#box(raw("CYC", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.cpu.pc", lang: ""))\
#box(raw("top.cpu.wb_", lang: ""))#box(raw("STB", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.cpu.wb_", lang: ""))#box(raw("ACK", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.cpu.wb_", lang: ""))#box(raw("CYC", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(text(raw("top.clk"), fill: unfocused_color))\
#box(text(raw("top.accelerator.state"), fill: unfocused_color))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(raw("STB", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(raw("ACK", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(raw("CYC", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.pc"), fill: unfocused_color))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(raw("STB", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(raw("ACK", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(raw("CYC", lang: ""))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(5, [#box(text(raw("top.clk"), fill: unfocused_color))\
#box(text(raw("top.accelerator.state"), fill: unfocused_color))\
#box(raw("top.accelerator.wb_", lang: ""))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.accelerator.wb_", lang: ""))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.accelerator.wb_", lang: ""))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.pc"), fill: unfocused_color))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.wb_"), fill: unfocused_color))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(6, [#box(text(raw("top.clk"), fill: unfocused_color))\
#box(text(raw("top.accelerator.state"), fill: unfocused_color))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.accelerator.wb_"), fill: unfocused_color))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("top.cpu.pc"), fill: unfocused_color))\
#box(raw("top.cpu.wb_", lang: ""))#box(text(raw("STB"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.cpu.wb_", lang: ""))#box(text(raw("ACK"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("top.cpu.wb_", lang: ""))#box(text(raw("CYC"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
