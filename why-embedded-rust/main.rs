struct Thing{}

fn take_ref<'a>(t: &'a mut Thing) -> &'a mut Thing {
    t
}

fn main() {
    // a owns itself
    let mut a = Thing{};

    // Create a reference to a
    let a_ref = take_ref(&mut a);

    // *Move* a to b
    let b = a;

    // A has moved, error
    *a_ref = Thing{};
}
