#import "../theme.typ": *
#import "../highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(text(raw("-"), fill: rgb(0, 0, 0, 0)))\
#box(raw("struct port Wishbone {", lang: "spade"))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("addr: &mut int<24>,", lang: "spade"))\
#box(raw("  write: &mut Option<int<16>>,", lang: "spade"))\
#box(raw("  data: &int<16>,", lang: "spade"))\
#box(raw("  ...", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("}", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(raw("#[wal_traceable(uses_clk, uses_rst)]", lang: "spade"))#box(text(raw("-"), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("struct port Wishbone {"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("addr: &mut int<24>,"), fill: unfocused_color))\
#box(text(raw("  write: &mut Option<int<16>>,"), fill: unfocused_color))\
#box(text(raw("  data: &int<16>,"), fill: unfocused_color))\
#box(text(raw("  ..."), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(raw("#[wal_traceable(uses_clk, uses_rst)]", lang: "spade"))#box(text(raw("-"), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("struct port Wishbone {"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("addr: &mut int<24>,", lang: "spade"))\
#box(raw("  write: &mut Option<int<16>>,", lang: "spade"))\
#box(raw("  data: &int<16>,", lang: "spade"))\
#box(raw("  ...", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(raw("#[wal_traceable(uses_clk, uses_rst)]", lang: "spade"))#box(text(raw("-"), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("struct port Wishbone {"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("addr: &mut int<24>,", lang: "spade"))\
#box(raw("  write: &mut Option<int<16>>,", lang: "spade"))\
#box(raw("  data: &int<16>,", lang: "spade"))\
#box(raw("  ...", lang: "spade"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(5, [#box(raw("#[wal_traceable(uses_clk, uses_rst)]", lang: "spade"))#box(text(raw("-"), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("struct port Wishbone {"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("addr: &mut int<24>,"), fill: unfocused_color))\
#box(text(raw("  write: &mut Option<int<16>>,"), fill: unfocused_color))\
#box(text(raw("  data: &int<16>,"), fill: unfocused_color))\
#box(text(raw("  ..."), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("}"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
