// Get polylux from the official package repository
#import "theme.typ": *
#import "highlight.typ": *

#set text(size: 25pt, font: "Noto Sans")

#show raw: set text(fill: rgb(128, 200, 255))

#show: university-theme.with(
  progress-bar: false
)

#show: highlights

// #let both_logos(height) = [#box(image("liu_logo.svg", height: height)) #box(image("JKU_Logo.svg", height: height))]

// #show raw.where(lang: "spade"): it => [
//   #show regex("\b(entity|let|inst|end)\b") : keyword => text(weight:"bold", keyword)
//   #it
// ]

#title-slide(
  authors: ("Frans Skarman"),
  title: "Utökning av Swim – ett byggsystem för moderna hårdvarubeskrivande språk",
  institution-name: [Linköping University]
)

#slide([
  #set align(center + horizon)
  #uncover("1-", [Hårdvarubeskrivande språk är fast på 90-talet!])

  #only(1, image("./software_languages.svg"))
  #only(2, image("./hardware_languages.svg"))

  #v(1cm)
])


#slide([
  #set align(center + horizon)

  #v(2em)

  #set text(size: 35pt)
  Spade #box(image("./spadefish_outline.svg", height: 0.8em)) 

  #set text(size: 25pt)
  Ett nytt HDL inspirerat av moderna mjukvaruspråk
])

#slide([
  #box([
    #set text(size: 35pt)
    `swim`
  ])

  Byggsystemet för Spade

  Jämför med `npm`, `python` eller `cargo`

  - Kompilerar koden
  - Installerar dependencies
  - Installerar kompilatorer
  - Programmerar hårdvaran


])

#slide([
  #set align(center)
  Ren install till testbar kod på 2 kommandon
  ```bash
  swim install-tools
  swim upload
  ```
])


#slide([
  Hårdvara är fast på 90-talet, så det här är relativt revolutionerande!

  #uncover("2-", [
    "Jag vill använda Swim som byggsystem utan Spade"
] )

  #uncover(3, [Här kommer ni in])
])

#slide([
  #line-by-line([
    - Se till att Swim kan köras utan Spade
    - Lägg till support för andra moderna HDL-språk
    - Gör allt generiskt så nya språk är lätta att lägga till
  ])

])

#slide([
  Vill ni utforska lite extra? Hur kan man kombinera generisk kod mellan språk?

  Tänk C++-templates anropar Java-generics anropar dynamiskt typad python
])

#slide([
  Teknik:

  Swim är skrivet i *Rust*, det blir huvudspråket

  Allt är *Open Source*


  Massor alternativ till alternativa HDL:er

  - Amaranth - *Python*
  - Chisel eller Spinal - *Scala*
  - Clash - *Haskell*
  - ROHD - *Dart*
  - ...

  Hårdvarukunskap är inget krav. Att några klarat datorkonstruktionen räcker :)
])

