// Get polylux from the official package repository
#import "theme.typ": *
#import "highlight.typ": *

#set text(size: 25pt, font: "Noto Sans")

#show raw: set text(fill: rgb(128, 200, 255))

#show: university-theme.with(
  progress-bar: false
)

#show: highlights

// #let both_logos(height) = [#box(image("liu_logo.svg", height: height)) #box(image("JKU_Logo.svg", height: height))]

// #show raw.where(lang: "spade"): it => [
//   #show regex("\b(entity|let|inst|end)\b") : keyword => text(weight:"bold", keyword)
//   #it
// ]

#title-slide(
  authors: ("Frans Skarman"),
  title: "Hur bygger man en dator?",
  institution-name: []
)

#slide(title: [Ett vanligt program], [
  Skapa ett program som beräknar

  `a = b + c`

  #uncover("2-", [Lite magi])

  #uncover("3-", [
    ```
    hämta a
    hämta b
    addera a + b
    spara resultatet i c
    ```
  ])
])

#slide(title: [En beskrivning av ett chip], [
  #grid(columns: (50%, 50%), [
    Skapa en krets som beräknar

    `a = b + c`

    #uncover("2-", [Stora mängder magi])
  ],[
    #only("3")[#image("./chip_original.svg")]
    #only("4")[#image("./chip_no_inputs.svg")]
    #only("5")[#image("./chip_no_output.svg")]
    #only("6")[#image("./chip_full.svg")]
  ])
])

#slide([

  #v(3cm)
  #grid(columns: (50%, 50%), [
    Skapa ett program som beräknar

    `a = b + c`
  ],[
    Skapa en krets som beräknar

    `a = b + c`
  ])

  #set align(center + horizon)
  #v(1cm)
  *Beskrivningen är samma!*
])


#slide([
  #set align(center + horizon)
  #uncover("1-", [*Programbeskrivning har utvecklats enormt sen 90-talet*])

  #only(1, image("./software_languages.svg"))
  #only(2, image("./hardware_languages.svg"))

  #v(1cm)
  #only(3, image("saker_att_sno.svg", height:75%))
  #only(4, image("snodda_saker.svg", height:75%))
])

#slide([
  #set align(center + horizon)

  Lättare beskrivning av datorer som är

  - Korrekta
  - Effektiva
  - Fungerande
])
