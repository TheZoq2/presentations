#let rgbf(r, g, b) = rgb(int(r*255), int(g*255), int(b*255))

#let spadeKeyword = rgbf(0.5, 1.0, 1.0)

#let typeColor= rgbf(0.67, 0.81, 0.17)
#let conditionalColor = rgbf(0.94, 0.63, 0.94)
#let builtinColor = rgbf(1.0, 0.37, 0.68)
#let operatorColor = rgbf(0.88, 0.65, 0.10)
#let typeOperatorColor = rgbf(0.88, 0.65, 0.10)
#let stringColor = rgbf(0.55, 0.71, 0.33)
#let boolLiteralColor = rgbf(0.88, 0.65, 0.10)
#let selfColor = rgbf(0.88, 0.34, 0.62)
#let useColor = rgbf(1.0, 0.6, 0.6)
#let attributeColor = rgbf(0.88, 0.34, 0.88)
#let commentGreen = rgbf(0, 0.8, 0.5)
#let bgColor = rgbf(0,0,0)
#let errorRed = rgbf(1.0,0.25,0.25)
#let errorBlue = rgbf(0.2, 0.7,1.0)
#let errorOrange = rgbf(0.72,0.62,0.03)
#let OliveGreen = rgbf(0,0.6,0)

#let kw2regex(keywords) = {
  regex("\b(" + keywords.join("|") + ")\b")
}


#let symbols2regex(symbols) = {
  regex("(" + symbols.join("|") + ")")
}

#let highlights(doc) = [
  #show raw.where(lang: "spade"): it => [
    #show kw2regex(
      (
        "entity",
        "fn",
        "reg",
        "let",
        "reset",
        "inst",
        "enum",
        "decl",
        "struct",
        "use",
        "as",
        "mod",
        "stage",
        "pipeline",
        "assert",
        "port",
        "set",
        "impl"
      )
    ) : keyword => text( weight:"bold", fill: spadeKeyword, keyword)

    #show kw2regex(
      ("int","bool","Option","clock")
    ) : w => text(fill: typeColor, w)

    #show kw2regex(
      ("match", "if", "else")
    ) : w => text(weight: "bold", fill: conditionalColor, w)

    #show kw2regex(("Some", "None")) : w => text(weight: "bold", fill: builtinColor, w)

    #show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&")
    ) : w => text(weight: "bold", fill: operatorColor, w)

    #show kw2regex(
      ("true", "false")
    ) : w => text(fill: boolLiteralColor, w)

    #show kw2regex(
      ("&", "mut")
    ) : w => text(fill: typeOperatorColor, w)


    #show kw2regex(
      ("self",)
    ) : w => text(fill: selfColor, w)

    #show regex("//.*") : w => text(fill: commentGreen, w)

    #show regex("#\[[^\]]*\]") : w => text(fill: attributeColor, w)

    // morecomment=[l]{//}, % l is for line comment
    // morecomment=[s]{/*}{*/}, % s is for start and end delimiter
    // commentstyle=\color{commentGreen},
    // morestring=[b]", % defines that strings are enclosed in double quotes
    // escapechar=\%,
    #it
  ]


  #show raw.where(lang: "rust_"): it => [
    #show kw2regex(
      (
        "entity",
        "fn",
        "reg",
        "let",
        "reset",
        "inst",
        "enum",
        "decl",
        "struct",
        "use",
        "as",
        "mod",
        "stage",
        "pipeline",
        "assert",
        "port",
        "set",
        "impl",
        "for",
        "in",
        "pub"
      )
    ) : keyword => text( weight:"bold", fill: spadeKeyword, keyword)

    #show kw2regex(
      ("int","bool","Option","clock", "Result", "i32", "u32", "String")
    ) : w => text(fill: typeColor, w)

    #show kw2regex(
      ("match", "if", "else")
    ) : w => text(weight: "bold", fill: conditionalColor, w)

    #show kw2regex(("Some", "None", "Ok", "Err")) : w => text(weight: "bold", fill: builtinColor, w)

    #show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&")
    ) : w => text(weight: "bold", fill: operatorColor, w)

    #show symbols2regex(
      ("\w+::",)
    ) : w => text(fill: useColor, w)

    #show kw2regex(
      ("true", "false")
    ) : w => text(fill: boolLiteralColor, w)

    #show kw2regex(
      ("&", "mut")
    ) : w => text(fill: typeOperatorColor, w)


    #show kw2regex(
      ("self",)
    ) : w => text(fill: selfColor, w)

    #show regex("//.*") : w => text(fill: commentGreen, w)
    #show regex("\*\*\*.*") : w => text(fill: errorBlue, w)
    #show regex("\^\^\^.*") : w => text(fill: errorRed, w)

    #show regex("#\[[^\]]*\]") : w => text(fill: attributeColor, w)

    // morecomment=[l]{//}, % l is for line comment
    // morecomment=[s]{/*}{*/}, % s is for start and end delimiter
    // commentstyle=\color{commentGreen},
    // morestring=[b]", % defines that strings are enclosed in double quotes
    // escapechar=\%,
    #it
  ]


  #show raw.where(lang: "c_"): it => [
    #show kw2regex(
      ("uint8_t",)
    ) : w => text(fill: typeColor, w)

    #show regex("\"[^\"]*\"") : w => text(fill: stringColor, w)

    #show symbols2regex(
      ("=", "-", "<", ">", "\+", "\|", ">>", "&")
    ) : w => text(weight: "bold", fill: operatorColor, w)

    #it
  ]

  #show raw.where(lang: "wal_analysis"): it => [
    #show regex("ANALYSIS") : w => text(fill: commentGreen, w)

    // morecomment=[l]{//}, % l is for line comment
    // morecomment=[s]{/*}{*/}, % s is for start and end delimiter
    // commentstyle=\color{commentGreen},
    // morestring=[b]", % defines that strings are enclosed in double quotes
    // escapechar=\%,
    #it
  ]


  #show raw.where(lang: "wal"): it => [
    #show kw2regex((
      "alias",
      "seta",
      "list",
      "when",
      "in-group",
      "in-groups",
      "groups",
      "print",
      "reval",
      "resolve-group",
      "step",
      "load",
      "find",
      "append",
      "while",
      "geta/default",
      "for/listextern",
      "call",
      "set",
      "do",
      "-",
      "min",
      "max",
      "average",
      "defun",
      "require",
      "printf",
      "count",
      "fdiv",
      "unload",
      "let",
      "cond",
      "lambda",
      "inc",
      "unalias",
      "get",
      "array",
      "mapa",
      "length",
      "map",
      "sum",
      "slice",
      "resolve-scope",
      "in-scope",
      "all-scopes",
      "timeframe",
      "sample-at",
      "defmacro",
      "macroexpand",
      "geta",
      "default",
      "in-spade-structs",
      "in-spade-struct",
      "spade-struct",
      "spade-structquasiquote",
      "spade/translate",
      "unquote",
      "quote",
      "fold",
      "rest",
      "fn",
      "reverse",
      "in",
      "define"
    )): w => text(fill: spadeKeyword, weight: "bold", w)

    // These don't work for some reason
    #show symbols2regex(("@",)) : w => text(fill: rgbf(0.7, 0.0, 0.0), w)
    #show symbols2regex(("#",)) : w => text(fill: rgbf(0.7, 0.0, 0.4), w)
    #show kw2regex((
      ";",
      "INDEX",
      "TS",
      "CG",
      "CS",
      "stage"
    )) : w => text(fill: rgbf(0.8, 0.3, 0.0), weight: "bold", w)
    #show kw2regex((
      "if",
      "unless",
      "for",
      "whenever"
    )) : w => text(fill: conditionalColor, weight: "bold", w)
    #show kw2regex(
      ("value","update","stall","flush"),
    ) : w => text(fill: OliveGreen, w)
    #show kw2regex(
      ("defsig", "new-trace")
    ) : w => text(fill: red, w)

    #show regex("\"[^\"]*\"") : w => text(fill: stringColor, w)

    #it
  ]

  #doc
]

