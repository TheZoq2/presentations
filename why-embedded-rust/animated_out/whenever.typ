#import "../theme.typ": *
#import "../highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(raw("(set transactions 0)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(whenever (&& (rising clk) ACK STB CYC)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("(inc transactions))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(print \"Number of transactions: \" transactions)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(raw("(set transactions 0)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(whenever (&& (rising clk) ACK STB CYC)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(inc transactions))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"Number of transactions: \" transactions)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(text(raw("(set transactions 0)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(whenever (&& (rising clk) ACK STB CYC)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(inc transactions))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"Number of transactions: \" transactions)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(text(raw("(set transactions 0)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(whenever (&& (rising clk) ACK STB CYC)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("(inc transactions))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(print \"Number of transactions: \" transactions)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(5, [#box(text(raw("(set transactions 0)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("(whenever (&& (rising clk) ACK STB CYC)"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(inc transactions))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(print \"Number of transactions: \" transactions)", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
