#import "../theme.typ": *
#import "../highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(raw("(define instances", lang: "wal"))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("(spade-struct wishbone::Wishbone", lang: "wal"))\
#box(raw("      [clk, rst, ack, stb, cyc])", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("(", lang: "wal"))#box(raw("in-spade-structs", lang: "wal"))\
#box(raw("  wishbone::Wishbone instances", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(raw("  (let [transactions 0]", lang: "wal"))\
#box(raw("    (whenever (&& (rising", lang: "wal"))#box(raw(" #clk", lang: "wal"))#box(raw(")", lang: "wal"))#box(raw(" #stb #ack #cyc", lang: "wal"))#box(raw(")", lang: "wal"))\
#box(raw("      (inc transactions)))", lang: "wal"))\
#box(raw("  (log/analysis CG \": \" transactions))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(text(raw("(define instances"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("(spade-struct wishbone::Wishbone", lang: "wal"))\
#box(raw("      [clk, rst, ack, stb, cyc])", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("("), fill: unfocused_color))#box(text(raw("in-spade-structs"), fill: unfocused_color))\
#box(text(raw("  wishbone::Wishbone instances"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  (let [transactions 0]"), fill: unfocused_color))\
#box(text(raw("    (whenever (&& (rising"), fill: unfocused_color))#box(text(raw(" #clk"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))#box(text(raw(" #stb #ack #cyc"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("      (inc transactions)))"), fill: unfocused_color))\
#box(text(raw("  (log/analysis CG \": \" transactions))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(text(raw("(define instances"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(spade-struct wishbone::Wishbone"), fill: unfocused_color))\
#box(text(raw("      [clk, rst, ack, stb, cyc])"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("("), fill: unfocused_color))#box(raw("in-spade-structs", lang: "wal"))\
#box(raw("  wishbone::Wishbone instances", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  (let [transactions 0]"), fill: unfocused_color))\
#box(text(raw("    (whenever (&& (rising"), fill: unfocused_color))#box(text(raw(" #clk"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))#box(text(raw(" #stb #ack #cyc"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("      (inc transactions)))"), fill: unfocused_color))\
#box(text(raw("  (log/analysis CG \": \" transactions))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(text(raw("(define instances"), fill: unfocused_color))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(spade-struct wishbone::Wishbone"), fill: unfocused_color))\
#box(text(raw("      [clk, rst, ack, stb, cyc])"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("("), fill: unfocused_color))#box(text(raw("in-spade-structs"), fill: unfocused_color))\
#box(text(raw("  wishbone::Wishbone instances"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("  (let [transactions 0]"), fill: unfocused_color))\
#box(text(raw("    (whenever (&& (rising"), fill: unfocused_color))#box(raw(" #clk", lang: "wal"))#box(text(raw(")"), fill: unfocused_color))#box(raw(" #stb #ack #cyc", lang: "wal"))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("      (inc transactions)))"), fill: unfocused_color))\
#box(text(raw("  (log/analysis CG \": \" transactions))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
