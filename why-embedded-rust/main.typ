// Get polylux from the official package repository
#import "theme.typ": *
#import "highlight.typ": *

#set text(size: 25pt, font: "Noto Sans")

#show: university-theme.with(
  progress-bar: false
)

#show: highlights

// #let both_logos(height) = [#box(image("liu_logo.svg", height: height)) #box(image("JKU_Logo.svg", height: height))]

// #show raw.where(lang: "spade"): it => [
//   #show regex("\b(entity|let|inst|end)\b") : keyword => text(weight:"bold", keyword)
//   #it
// ]

#title-slide(
  authors: ("Frans Skarman"),
  title: "Why Embedded Rust?",
  institution-name: [],
  logo: image("ferris_chip.svg", height:40%)
)



#show strong: set text(fill: rgb(251, 94, 0))
#show raw.where(lang: none): set text(fill: rgb(251, 200, 150))

#slide(title: [About me], [
  #grid(columns: (50%, 50%), [
    #line-by-line(
      [
        - Rust evangelist #box(image("ferris_exorcism.svg", height: 1em))
        - Previous co-maintainer of `stm32f1xx_hal`
        - Not super involved in embedded anymore :(
        - 5th PhD student at LIU
          - Research on programming tools for FPGAs
          - spade-lang written in and inspired by Rust
        - Likes sailing
      ]
    )

  ],
  [
    #only(5, image("frans.jpg"))
  ])
])


#slide([
  #set align(center + horizon)
  #set text(size: 40pt)

  #v(3em)

  Who are you?

  #set align(right)
  #image("rustacean-flat-happy.svg", height:30%)
])

#slide([
  #set align(center + horizon)
  #set text(size: 40pt)

  #v(3em)

  Why Rust?

  #set align(right)
  #image("ferristhink.svg", height:30%)
])

#slide(title: [Memory Safety!], [
  #grid(columns: (50%, 50%), [
    #uncover("1-", [
      We get
      - No memory corruption
      - No segmentation faults
      - No race conditions
        - Free safe multithreading
    ])
  ])
])

#slide(title: [Memory Safety!], [
  #grid(columns: (45%, 65%), [
      #uncover("1-", [
        Every value has

        - A single *owner*
        - *One mutable* reference or
        - *Several immutable* references

        #uncover("3-", [
          Similar to `std::unique_ptr`
        ])
      ])
    ],
    [
      #uncover("2-", [

        ```rust_
        // a owns itself
        let mut a = Thing::new();

        // Create a reference to a
        let a_ref = take_ref(&mut a);
            ^^^^^ Live until end of scope

        // *Move* a to b
        let b = a;

        // A has moved, error
        *a_ref = 10;
        ```
      ])
    ]
  )
])


#slide(title: [Memory Safety Again], [
  We don't get
  - Zero memory leaks (technically)
  - Deadlock protection

  #uncover(2, [Still, a whole lot better than C or C++!])

  #set align(right)
  #image("corro.svg", height: 35%)
])



#slide([
  #set align(center + horizon)
  #set text(size: 40pt)

  #v(2em)
  
  Hot take: Memory safety is the least interesting part of Rust

  #set align(left)
  #image("ferrishmm.png", height: 35%)
])

#slide(title: [Type System], [
  ```rust_
  enum Result<T, E> {
    Ok(T),
    Err(E)
  }

  fn could_fail() -> Result<i32, String> {..}
  fn consume_int(i32) {..}

  fn process() {
    let x = could_fail();
    consume_int(x); // Compiler error
  }
  ```

  #v(-5em)
  #set align(right)
  #only(2, image("rustacean-flat-happy.svg", height:30%))
  #only(3, image("ferris_deal_with_it.svg", height:30%))
])

#slide(title: [Type System], [
  #grid(columns: (50%, 50%), [

    #line-by-line([
      - "Newtypes" are very common
        - Prefer semantic meaning over technical representation
    ])
    #line-by-line([
      - "If it compiles, it runs"
    ], start: 3)
  ],
  [
    #h(1em)
    #uncover(2,
      ```rust_
      struct Identifier(pub String);

      struct BitString(pub String);
      ```
    )
  ])
])

#slide(title: [A Type System Anecdote], [
  #grid(columns: (50%, 50%), [
      Refactoring Rust:

      #line-by-line([
        - Randomly make a change
        - Compile
        - Fix compiler error
        - Repeat
      ])

      #v(1em)

      #line-by-line([
        - $>8$ hours of refactoring
        - $<5$ hours of sleep
        - Worked on the first try\*
      ], start:6)
    ],
    [
      #only("5-", image("./refactor.png"))
    ])
])

#slide(title: [Type Safety], [
  C-quiz!


  ```c_
  uint8_t a = 99;
  uint8_t b = 100;
  uint8_t c = a - b;
  printf("%u, %u\n", a-b, c)
  ```

  #v(1em)
  #uncover("2-", [
    - `a-b: 4294967295 c: 255`
    - `a-b: ffffffff   c: ff`
  ])

])

#slide(title: [Type Safety], [
  Rust prefers being explicit about any type conversions

  #grid(columns: (50%, 50%), [
    ```rust_
    let x: i32 = ..;
    let y: u32 = ..;
    x + y // Type error!
    ```
  ], [
    #only("2-", image("./fig/int_error.png", width: 100%))
  ])


  #v(-5em)
  #only(3, image("rustacean-flat-happy.svg", height:30%))
  #only(4, image("ferris_deal_with_it.svg", height:30%))
])

#slide(title: [Tooling], [
  #grid(columns: (50%, 50%), [
    #line-by-line([
    - Cargo
    - Helpful compiler errors
  ])
  #line-by-line([
    - Good IDE integration via LSP
      - VSCode
      - Vim
      - Emacs
      - Intellij
    - Documentation generation
      - Online docs
      - Libraries are generally well documented
    ], start: 4)
  ],[
    #only(1, [
      ```toml
      colored = "2.0"
      indoc = "2.0"
      insta = "1.14.0"
      itertools = "0.10.3"
      local-impl = {
        git = "..."
      }
      logos = "0.13.0"
      num = {
        version = "0.4.0",
        features = ["serde"]
      }
      ```
    ])
    #only(2, image("./fig/lifetime_error.png"))
    #only(3, image("./fig/error_issue.png"))
    #only(5, image("./fig/docs.rs.png"))
  ])
])

#slide([
  #set align(center + horizon)
  #set text(size: 40pt)

  #v(3em)

  *Embedded*, Finally

  #set align(right)
  #image("ferris_chip.svg", height:40%)
])

#slide(title: [Ownership and borrowing], [
  #line-by-line([
    - No OS
      - #strike[Segfaults] Memory corruption becomes more annoying
    - Ownership also works for physical devices
      - Pins
      - Peripherals
  ])

  #uncover(3, [
    ```rust_
    let uart = Uart1.setup(gpioa.pa0, ...);
                           ********* Moved here before
    let spi = SPI1.setup(gpioa.pa0, ...);
                         ^^^^^^^^^ Used here
    ```
  ])
])

#slide(title: [Type Safety], [
  #line-by-line([
    - Debugging is harder in embedded
    - Catching problems early in the type system is good
  ])
])


#slide(title: [High level code], [
  ```rust_
  for event in c
      .local
      .debouncer
      .events(c.local.matrix.get().get())
      .chain(encoder_event.into_iter())
      .map(c.local.transform)
  {
      for &b in &ser(event) {
          block!(c.local.tx.write(b)).get();
      }
      handle_event::spawn(event).unwrap();
      led_handle_event::spawn(event).unwrap();
  }
  ```
])

#slide(title: [ARTIC], [
  #set text(size:20pt)

  ```rust_
    #[task(binds = USART3, priority = 5, local = [rx, buf])]
    fn rx(c: rx::Context) { ... }

    #[task(binds = USB_HP_CAN_TX, priority = 4, shared = [usb_dev, usb_class])]
    fn usb_tx(c: usb_tx::Context) { ... }

    #[task(binds = USB_LP_CAN_RX0, priority = 4, shared = [usb_dev, usb_class])]
    fn usb_rx(c: usb_rx::Context) { .. }

    #[task(priority = 3, capacity = 8, shared = [layout])]
    fn handle_event(mut c: handle_event::Context, event: Event) { .. }

    #[task(priority = 3, shared = [usb_dev, usb_class, layout], local=[timeout])]
    fn tick_keyberon(mut c: tick_keyberon::Context) { .. }
    ```
])

#slide(title: [Tooling], [
  #line-by-line([
    - Cross comilation is trivial
      - `rustup target add <arch>`
      - `cargo build --target <arch>`
    - `probe-rs` handles uploading, debug prints etc.
    ])
])

#slide([
  #set align(center + horizon)
  #set text(size: 40pt)

  #v(3em)
  
  Uncharted lands #only(2, [by me at least :)])
])

#slide(title: [Async], [
  - Rust's async/await allows for unprecedently easy and efficient multitasking in embedded systems.
  - It obsoletes the need for a traditional RTOS
  - *and is faster and smaller than one!*
])


#slide(title: [Ferrocene], [
  > Ferrocene is an open source *qualified Rust compiler toolchain*. With this,
  Ferrous Systems invested its decade of Rust experience to make Rust a
  first-class language for mission-critical and functional safety systems.

  > For its first release, Ferrocene 23.06 is a *ISO 26262 (ASIL D)* and *IEC 61508
  (SIL 4)* qualified version of the existing open-source compiler, rustc, based
  on Rust 1.68.

  We plan to work on standards like DO-178C, ISO 21434, and IEC 62278 in the future.
])


#slide([
  #set align(center + horizon)
  #set text(size: 40pt)

  #v(3em)
  
  Why *not* Rust?
])



#slide(title: [Why not rust?],
line-by-line[
  - Poor (but growing) ecosystem support
  - Embedded support is spotty
    - A well supported microcontroller is great
    - Otherwise you'll be in pain
  - Not all architectures are supported
    - LLVM covers many
    - Notable exceptions like AVR
])

#slide(title: [Why not rust?],
line-by-line[
  - High Learning Curve
    - You won't write incorrect code
    - But you will be frustrated
])


#slide(title: [Living with the compiler], [
  Some advice:
  - The compiler is harsh, especially in the beginning
  #only("2", [
    - Write Rust the way it wants you to
      - Avoid long references where possible
      - Especially in structs
      - Performance is secondary, use `clone`
  ])
  #only("3", [
    - *Write Rust the way it wants you to*
      - Avoid long references where possible
      - Especially in structs
      - Performance is secondary, use `clone`
  ])
])


#slide(title: [Some resources], [
  General

  - *Official book and stuff* https://www.rust-lang.org/learn
  - *Rustlings* https://github.com/rust-lang/rustlings

  Embedded
  - *Embedded book* https://doc.rust-lang.org/stable/embedded-book/
  - *An Overview of the Embedded Rust Ecosystem*
    - https://www.youtube.com/watch?v=vLYit_HHPaY
  - *nrf-hal* https://github.com/nrf-rs/nrf-hal
])
