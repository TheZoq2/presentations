#import "../theme.typ": *
#import "../highlight.typ": *

#set par(leading: 0.5em)

#let unfocused_color = rgb(60%, 60%, 60%)

#only(1, [#box(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("(define transactions = 0)", lang: "wal"))\
#box(raw("  (whenever (&& (rising top.clk) ", lang: "wal"))#box(raw(" #ACK #STB #CYC", lang: "wal"))#box(raw(")", lang: "wal"))\
#box(raw("    (inc transactions))", lang: "wal"))\
#box(raw("  (print", lang: "wal"))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(raw("CG", lang: "wal"))#box(raw(" \" performed \" transactions \" transactions\")", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(2, [#box(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(define transactions = 0)"), fill: unfocused_color))\
#box(text(raw("  (whenever (&& (rising top.clk) "), fill: unfocused_color))#box(text(raw(" #ACK #STB #CYC"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("    (inc transactions))"), fill: unfocused_color))\
#box(text(raw("  (print"), fill: unfocused_color))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(text(raw("CG"), fill: unfocused_color))#box(text(raw(" \" performed \" transactions \" transactions\")"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(3, [#box(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(define transactions = 0)"), fill: unfocused_color))\
#box(text(raw("  (whenever (&& (rising top.clk) "), fill: unfocused_color))#box(text(raw(" #ACK #STB #CYC"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("    (inc transactions))"), fill: unfocused_color))\
#box(text(raw("  (print"), fill: unfocused_color))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(text(raw("CG"), fill: unfocused_color))#box(text(raw(" \" performed \" transactions \" transactions\")"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(4, [#box(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(define transactions = 0)"), fill: unfocused_color))\
#box(text(raw("  (whenever (&& (rising top.clk) "), fill: unfocused_color))#box(text(raw(" #ACK #STB #CYC"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("    (inc transactions))"), fill: unfocused_color))\
#box(text(raw("  (print"), fill: unfocused_color))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(text(raw("CG"), fill: unfocused_color))#box(text(raw(" \" performed \" transactions \" transactions\")"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(5, [#box(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(define transactions = 0)"), fill: unfocused_color))\
#box(text(raw("  (whenever (&& (rising top.clk) "), fill: unfocused_color))#box(text(raw(" #ACK #STB #CYC"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("    (inc transactions))"), fill: unfocused_color))\
#box(text(raw("  (print"), fill: unfocused_color))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(text(raw("CG"), fill: unfocused_color))#box(text(raw(" \" performed \" transactions \" transactions\")"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(6, [#box(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(define transactions = 0)"), fill: unfocused_color))\
#box(text(raw("  (whenever (&& (rising top.clk) "), fill: unfocused_color))#box(text(raw(" #ACK #STB #CYC"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("    (inc transactions))"), fill: unfocused_color))\
#box(text(raw("  (print"), fill: unfocused_color))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(text(raw("CG"), fill: unfocused_color))#box(text(raw(" \" performed \" transactions \" transactions\")"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(7, [#box(text(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(raw("(define transactions = 0)", lang: "wal"))\
#box(raw("  (whenever (&& (rising top.clk) ", lang: "wal"))#box(raw(" #ACK #STB #CYC", lang: "wal"))#box(raw(")", lang: "wal"))\
#box(raw("    (inc transactions))", lang: "wal"))\
#box(raw("  (print", lang: "wal"))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(raw("CG", lang: "wal"))#box(raw(" \" performed \" transactions \" transactions\")", lang: "wal"))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(8, [#box(text(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(define transactions = 0)"), fill: unfocused_color))\
#box(text(raw("  (whenever (&& (rising top.clk) "), fill: unfocused_color))#box(raw(" #ACK #STB #CYC", lang: "wal"))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("    (inc transactions))"), fill: unfocused_color))\
#box(text(raw("  (print"), fill: unfocused_color))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(text(raw("CG"), fill: unfocused_color))#box(text(raw(" \" performed \" transactions \" transactions\")"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])

#only(9, [#box(text(raw("(in-groups (groups '(\"ACK\", \"STB\", \"CYC\"))"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw("--"), fill: rgb(0, 0, 0, 0)))#box(text(raw("(define transactions = 0)"), fill: unfocused_color))\
#box(text(raw("  (whenever (&& (rising top.clk) "), fill: unfocused_color))#box(text(raw(" #ACK #STB #CYC"), fill: unfocused_color))#box(text(raw(")"), fill: unfocused_color))\
#box(text(raw("    (inc transactions))"), fill: unfocused_color))\
#box(text(raw("  (print"), fill: unfocused_color))\
#box(text(raw("----"), fill: rgb(0, 0, 0, 0)))#box(raw("CG", lang: "wal"))#box(text(raw(" \" performed \" transactions \" transactions\")"), fill: unfocused_color))#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
#box(text(raw(""), fill: rgb(0, 0, 0, 0)))\
])
